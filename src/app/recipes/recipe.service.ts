import { Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Recipe } from './recipe.model';

@Injectable()
export class RecipeService {
  private recipes: Recipe[] = [
    new Recipe(
      'Pizza',
      'La pizza jambon champignons',
      'https://i0.wp.com/www.piccolericette.net/piccolericette/wp-content/uploads/2019/10/4102_Pizza.jpg?resize=895%2C616&ssl=1',
      [
        new Ingredient('Pate a pizza', 1),
        new Ingredient('Mozarella', 2),
        new Ingredient('Jambon', 1),
        new Ingredient('Champignons', 1),
      ]
    ),
    new Recipe(
      'Pâtes carbonara',
      'Tout est dans le titre -  que dire de plus',
      'https://res.cloudinary.com/swissmilk/image/fetch/w_1600,c_fill,g_auto,f_auto,q_auto:eco,ar_16:9/https://api.swissmilk.ch/wp-content/uploads/2019/06/spaghetti-carbonara-2560x1920.jpg',
      [
        new Ingredient('spaghetti', 1),
        new Ingredient('Oeuf', 3),
        new Ingredient('Parmesan', 1),
        new Ingredient('Lard', 1),
      ]
    ),
  ];

  constructor(private slService: ShoppingListService) {}

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(id: number) {
    return this.recipes[id];
  }

  addIngredientToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }
}
